#include<stdio.h>
void swap(int *x,int *y)
    {
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
    }
    
int main()
{
    int a,b;
    printf("enter any two number\n");
    scanf("%d%d",&a,&b);
    printf("\nThe number before swapping is\na=%d\nb=%d\n",a,b);
    swap(&a,&b);
    printf("\nThe number after swapping is\na=%d\nb=%d\n",a,b);
    return 0;
}
