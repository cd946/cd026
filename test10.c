#include<stdio.h>
int main()
{
    int x1,x2,sum,difference,multiply,divide,remainder;
    printf("enter any two values:\n");
    scanf("%d%d",&x1,&x2);
    add(&x1,&x2,&sum);
    printf("the sum of the two number is %d\n",sum);
    diff(&x1,&x2,&difference);
    printf("the difference of the two number is %d\n",difference);
    mult(&x1,&x2,&multiply);
    printf("the multiple of the two number is %d\n",multiply);
    div(&x1,&x2,&divide);
    printf("division of two number is %d\n",divide);
    rem(&x1,&x2,&remainder);
    printf("remainder of two number is %d\n",remainder);

    return 0;
}

void add(int *a,int *b,int *c)
{
    *c=*a+*b;
}
void diff(int *a,int *b,int *c)
{
    *c=*a-*b;
}
void mult(int *a,int *b,int *c)
{
    *c=(*a)*(*b);
}
void div(int *a,int *b,int *c)
{
    *c=(*a)/(*b);
}
void rem(int *a,int *b,int *c)
{
    *c=(*a)%(*b);
}
