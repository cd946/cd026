#include<stdio.h>
struct name
{
    char firstname[50];
    char lastname[50];
};


struct student
{
    int roll_no;
    struct name n;
    char department[30];
    float fees;
    char section[20];
    int totalmarks;
};

int main()
{
    int i;
    struct student s[2];
    for(i=0;i<2;i++)
    {
        printf("\n**----***---*** enter the details of student %d**----***---*** \n",i+1);
        printf("enter the roll number:\n");
        scanf("%d",&s[i].roll_no);
        printf("enter the student's first name:\n");
        scanf("%s",&s[i].n.firstname);
        printf("enter the student's last name:\n");
        scanf("%s",&s[i].n.lastname);
        printf("enter the section:\n");
        scanf("%s",&s[i].section);
        printf("enter the department:\n");
        scanf("%s",&s[i].department);
        printf("enter the fees:\n");
        scanf("%d",&s[i].fees);
        printf("enter the total marks out of 500:\n");
        scanf("%d",&s[i].totalmarks);
    }

    for(i=0;i<2;i++)
    {
        printf("\n\n___________detais of student %dth___________\n",i+1);
        printf("student's roll number=%d\n",s[i].roll_no);
        printf("employee name=%s %s\n",s[i].n.firstname,s[i].n.lastname);
        printf("section=%s\n",s[i].section);
        printf("department=%s\n",s[i].department);
        printf("fees=%d\n",s[i].fees);
        printf("total marks=%d\n",s[i].totalmarks);
    }


    if(s[0].totalmarks>s[1].totalmarks)
  {
        printf("\n\nthe marks obtained student 1st is highest whose details are:\n\n");
        printf("student's roll number=%d\n",s[0].roll_no);
        printf("employee name=%s %s\n",s[0].n.firstname,s[0].n.lastname);
        printf("section=%s\n",s[0].section);
        printf("department=%s\n",s[0].department);
        printf("fees=%d\n",s[0].fees);
        printf("total marks=%d\n",s[0].totalmarks);
  }
  else
  {
        printf("\n\nthe marks obtained by student 2nd is highest whose details are:\n\n");
        printf("student's roll number=%d\n",s[1].roll_no);
        printf("employee name=%s %s\n",s[1].n.firstname,s[1].n.lastname);
        printf("section=%s\n",s[1].section);
        printf("department=%s\n",s[1].department);
        printf("fees=%d\n",s[1].fees);
        printf("total marks=%d\n",s[1].totalmarks);
  }
    return 0;
}